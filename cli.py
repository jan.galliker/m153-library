from os import system, name
from datetime import datetime
from prompt_toolkit import prompt, print_formatted_text, HTML
from prompt_toolkit.shortcuts import button_dialog, yes_no_dialog
from prompt_toolkit.key_binding import KeyBindings
from validators import DateValidator, PasswordValidator, MinLengthValidator
import databaseDAO

class Cli:

    #defining bindings for prompt, this is because I would like a user to be able to close the application at any time
    bindings = KeyBindings()

    @bindings.add('escape')
    def _(event):
            exit()

    #database Access Object for all accesses to the Database
    dbDAO = databaseDAO.databaseDAO()

    #function to clear the display (works not a intened in VSC, rather use CMD)
    def clearScr(self):
        if name == 'nt':
            system('cls')
        else:
            system('clear')

    #by calling this function, the application will start
    def startApp(self):
        result = button_dialog(
            title='Welcome to the Library App',
            text='Do you already have an account?',
            buttons=[
                ('Yes', True),
                ('No', False),
                ('Exit', None)
            ],
        ).run()
        self.clearScr()
        print_formatted_text(HTML('''   <b>Welcome to the Library App!</b>\nAt any time you can stop the app by pressing 'ESC' '''))

        if result == None:
            return

        elif result:
            user = self.login()
        else:
            user = self.register()
        userId = user[0]
        if user[1]:
            self.employeeMenu(userId)
        else:
            self.userMenu(userId)

    #function for the login of a user
    def login(self):
        isValid = False
        while isValid == False:
            username = prompt('Please enter your Username:',key_bindings=self.bindings)
            password = prompt('Please enter your Password:', is_password=True, key_bindings=self.bindings)
            user = self.dbDAO.checkForUser(username,password)
            if  user != None:
                isValid = True
            else:
                print_formatted_text(HTML('<ansired>Invalid Login!</ansired>'))
                print_formatted_text('Please try again')
        print_formatted_text(user)
        return user

    #function for creation a new user
    def register(self):
        nameDoesntExist = False
        while not nameDoesntExist:
            username = prompt('Please enter a Username:',key_bindings=self.bindings,validator=MinLengthValidator(3), validate_while_typing=False)
            nameDoesntExist = not self.dbDAO.usernameExists(username)
            if not nameDoesntExist:
                self.clearScr()
                print_formatted_text(HTML('<ansired>Username already taken!</ansired>'))
                print_formatted_text('Please try again')            
        password = prompt('Please enter a Password:',key_bindings=self.bindings, is_password=True, validator=PasswordValidator(), validate_while_typing=False)
        user = self.dbDAO.createUser(username,password)
        print_formatted_text(user)
        return user

    #if the logged in user is an employee, he will land on this overview page
    def employeeMenu(self,userId):
            self.clearScr()
            while True:
                print_formatted_text(('''What do you want to modifiy:
    by pressing '1' you can add an Author entry
    by pressing '2' you can delete an Author entry
    by pressing '3' you can add a Book entry
    by pressing '4' you can delete a Book entry
    by pressing '5' you can change your password
    by pressing '6' you can quit the application'''))
                input = prompt("Your choice:",key_bindings=self.bindings)
            
                if input== '1':
                    self.addAuthor()
                elif input == '2':
                    self.removeAuthor()
                elif input == '3':
                    self.addBook()
                elif input == '4':
                    self.removeBook()
                elif input == '5':
                    self.resetPassword(userId)
                elif input == '6':
                    return
                else:
                    self.clearScr()
                    print_formatted_text(HTML('<ansired>Invalid Input!</ansired>'))
                    print_formatted_text('Please try again')

    #function to add an author to the database (only accessible for employees)
    def addAuthor(self):
        print_formatted_text("Adding new Author")
        firstname = prompt("Please enter the authors firstname:",validator=MinLengthValidator(2), validate_while_typing=False, key_bindings=self.bindings)
        lastname = prompt("Please enter the authors lastname:",validator=MinLengthValidator(2), validate_while_typing=False, key_bindings=self.bindings)
        country = None
        while(country is None):
            self.clearScr()
            allCountries = self.dbDAO.getAllCountries()
            country = self.displayEntities(allCountries,"Please select the country from where the author is from, by pressing the corresponding key","Number   Country")
            if(country is None):
                confirm = yes_no_dialog(
                    title="For Author '%s %s'" % (firstname, lastname),
                    text='Are you sure you want to leave Country empty?')
                if(confirm.run()):
                    country = (None,)
                    break
        self.clearScr()
        birthdateStr = prompt("Please enter the authors birthdate in the format 'dd.mm.yyyy':",validator=DateValidator(), validate_while_typing=False, key_bindings=self.bindings)
        birthdate = datetime.strptime(birthdateStr, '%d.%m.%Y').date()
        confirm = yes_no_dialog(
            title="Add Country to Author '%s %s'" % (firstname, lastname),
            text='Are you sure you want to add Author')
        if(not confirm.run()):
            print_formatted_text("Addition Canceled")
            return
        self.dbDAO.addAuthor(firstname,lastname,country[0],birthdate)
        print_formatted_text(HTML("<ansigreen>successfully added an author</ansigreen>"))
        return

    #function for removing an author from the database (only accessible for employees)
    def removeAuthor(self):
        self.clearScr()
        while True:
            allAuthors = self.dbDAO.getAllAuthors()
            selectedAuthor = self.displayEntities(allAuthors,"Please select an author to remove, by pressing the corresponding key","Number   Firstname Lastname   Country of origin   Birthdate")
            

            if selectedAuthor is None:
                return

            confirm = yes_no_dialog(
            title="Delete Author '%s %s'" % (selectedAuthor[1], selectedAuthor[2]),
            text='Are you sure to delete this author?')

            if(confirm.run()):
                self.dbDAO.deleteAuthor(selectedAuthor[0])
                self.clearScr()
                print_formatted_text(HTML("<ansigreen>successfully removed an author</ansigreen>"))
            else:
                self.clearScr()
                print_formatted_text(HTML("Deletion canceled"))

    #function to add a book to the database (only accessible for employees)
    def addBook(self):
        print_formatted_text("Adding new Book")
        title = prompt("Please enter the books title:",validator=MinLengthValidator(2), validate_while_typing=False, key_bindings=self.bindings)
        description = prompt("Please enter the books description:",validator=MinLengthValidator(50), validate_while_typing=False, key_bindings=self.bindings)
        author = None
        while(author is None):
            self.clearScr()
            allAuthors = self.dbDAO.getAllAuthors()
            author = self.displayEntities(allAuthors,"Please select the author who has written the book, by pressing the corresponding key","Number   Firstname Lastname   Country of origin   Birthdate")
            if(author is None):
                confirm = yes_no_dialog(
                    title="Add Author to Book '%s'" % title,
                    text='Are you sure that the book has no know author?')
                if(confirm.run()):
                    author = (None,)
                    break
        self.clearScr()
        firstReleasedStr = prompt("Please enter the Books first Releasedate in this format 'dd.mm.yyyy':",validator=DateValidator(), validate_while_typing=False, key_bindings=self.bindings)
        firstReleased = datetime.strptime(firstReleasedStr, '%d.%m.%Y').date()

        genre = None
        while(genre is None):
            allGenres = self.dbDAO.getAllGenres()
            genre = self.displayEntities(allGenres,"Please select the author who has written the book, by pressing the corresponding key","Number   Firstname Lastname   Country of origin   Birthdate")
            if(genre is None):
                print_formatted_text(HTML('<ansired>Each book must belong to a Genre!</ansired>'))
        
        confirm = yes_no_dialog(
            title="Add Book '%s'" % title,
            text='Are you sure you want to add the Book to the library')
        if(not confirm.run()):
            print_formatted_text("Addition Canceled")
            return
        self.dbDAO.addBook(title,description,firstReleased,author[0],genre[0])
        print_formatted_text(HTML("<ansigreen>successfully added a new book</ansigreen>"))
        return

    #function for removing a book from the database (only accessible for employees)
    def removeBook(self):
        while True:
            allBooks = self.dbDAO.getAllBooks()
            selectedBook = self.displayEntities(allBooks,"Please select a book to remove, by pressing the corresponding key","Number   Book Title   Book Description   Author   Genre   First Released")
            
            if selectedBook is None:
                return
            
            confirm = yes_no_dialog(
            title="Delete Book '%s'" % selectedBook[1],
            text='Are you sure to delete this book?')

            if(confirm.run()):
                self.dbDAO.deleteBook(selectedBook[0])
                self.clearScr()
                print_formatted_text(HTML("<ansigreen>successfully removed a book</ansigreen>"))
            else:
                self.clearScr()
                print_formatted_text(HTML("Deletion canceled"))

    #This is the overview page for the user after a successful login
    def userMenu(self,userId):
        self.clearScr()
        (booksBorrowed, overdueBooks) = self.dbDAO.getInfosAbout(userId)
        print_formatted_text(('''You are currently borrowing %s book(s).''' % booksBorrowed))
        if overdueBooks>0:
            print_formatted_text(HTML('''<ansired>%s Book(s) are overdue, please return them as soon as possible!</ansired>''' % overdueBooks))

        while True:
            print_formatted_text(('''These are your operations:
    by pressing '1' see a all books you are currently borrowing
    by pressing '2' see a all books that are available at the moment
    by pressing '3' search for all available books for a specific genre
    by pressing '4' you can change your password
    by pressing '5' you can quit the application'''))
            input = prompt("Your choice:",key_bindings=self.bindings)
            
            if input== '1':
                self.borrowedBooks(userId)
                self.clearScr()
            elif input == '2':
                self.availableBooks(userId)
                self.clearScr()
            elif input == '3':
                self.availableBooksByGenre(userId)
            elif input == '4':
                self.resetPassword(userId)
            elif input == '5':
                return
            else:
                self.clearScr()
                print_formatted_text(HTML('<ansired>Invalid Input!</ansired>'))
                print_formatted_text('Please try again')

    #gets all books that a user with the specified userid is borrowing at the moment 
    def borrowedBooks(self,userId):
        self.clearScr()
        while True:
            allBorrowedBooks = self.dbDAO.getBorrowedBooks(userId)
            returnbook = self.displayEntities(allBorrowedBooks,"Please select a book of yours that you would like to return, by pressing the corresponding key","Number   Book Title   Book Description   Author   Borrowing since")
            if returnbook is None:
                return
            self.dbDAO.returnBook(returnbook[0])
            self.clearScr()
            print_formatted_text(HTML("<ansigreen>Book returned successfully</ansigreen>"))

    #gets all books that are not being borrowed from anyone
    def availableBooks(self,userId):
        self.clearScr()
        while True:
            allAvailableBooks = self.dbDAO.getAvailableBooks()
            borrowbook = self.displayEntities(allAvailableBooks,"Please select a book of from the library that you would like to borrow, by pressing the corresponding key","Number   Book Title   Book Description   Author   Genre   First Released")
            if borrowbook is None:
                return
            self.dbDAO.borrowBook(borrowbook[0],userId)
            self.clearScr()
            print_formatted_text(HTML("<ansigreen>successfully borrowed a book</ansigreen>"))

    #get all books that are not being borrowed from anyone and belong to a certain genre
    def availableBooksByGenre(self,userId):
        self.clearScr()
        while True:
            AllGenres = self.dbDAO.getAllGenres()
            searchByGenre = self.displayEntities(AllGenres,"Please select a specific genre of books to filter for, by pressing the corresponding key","Number   Genre")
            if searchByGenre is None:
                return
            self.clearScr()
            allAvailableBooksByGenre = self.dbDAO.getAvailableBooksByGenre(searchByGenre[0])
            borrowbook = self.displayEntities(allAvailableBooksByGenre,"Please select a book of from the library that you would like to borrow, by pressing the corresponding key","Number   Book Title   Book Description   Author   Genre   First Released")
            if borrowbook is None:
                return
            self.dbDAO.borrowBook(borrowbook[0],userId)
            self.clearScr()
            print_formatted_text(HTML("<ansigreen>successfully borrowed a book</ansigreen>"))

    # set a new password for the specified userid
    def resetPassword(self,userId):
        self.clearScr()
        password = prompt('Please enter a new Password:',key_bindings=self.bindings, is_password=True, validator=PasswordValidator(), validate_while_typing=False)
        self.dbDAO.changePassword(userId,password)
        print_formatted_text(HTML("<ansigreen>Password has been changed</ansigreen>"))
        return

    #will display all entries of a tuple of an array in a paged fashion, to not overload the screen.
    #In addition you have to add a message that will be display and also the information about the entities 
    def displayEntities(self,tupleArray,message,entityInformation):
        counter = 0
        while True:
            print_formatted_text(entityInformation)
            if(len(tupleArray)==0):
                print_formatted_text("No Items available")
            for i in range(counter,min(counter+5,len(tupleArray))):
                print_formatted_text("%s.  " % i, end=" ")
                for j in range (1,len(tupleArray[i])):
                    print_formatted_text("%s  " % tupleArray[i][j], end=" ")
                print_formatted_text("")
            print_formatted_text(message)
            print_formatted_text("for going to the next page press 'n'\n for going to the previous page press 'p'\n if you want to exit press 'x'")
            input = prompt("your key:",key_bindings=self.bindings)
            if(input == 'x'):
                return None
            elif(input == 'n'):
                self.clearScr()
                if(counter + 5 >= len(tupleArray)):
                    print_formatted_text(HTML("<ansired>No more items available !</ansired>"))
                else:
                    counter += 5
            elif(input == 'p'):
                self.clearScr()
                if(counter - 5 < 0):
                    print_formatted_text(HTML("<ansired>No more items available !</ansired>"))
                else:
                    counter -= 5
            else:
                try:
                    if int(input) >= counter and int(input) < counter+5:
                        return tupleArray[int(input)]
                    self.clearScr()
                    print_formatted_text(HTML("<ansired>Invalid input!</ansired>"))
                except:
                    self.clearScr()
                    print_formatted_text(HTML("<ansired>Invalid input!</ansired>"))
    