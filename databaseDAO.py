from db import Db
from datetime import date, timedelta

class databaseDAO:

    db = Db()

    #with with functio you will initalize the whole setup on the database for the cli application
    def createTables(self):
        createTableCountryQuery = '''CREATE TABLE Country (
            id                            SERIAL PRIMARY KEY,
            name                          VARCHAR(255)
            );'''
        createTableAuthorQuery = '''CREATE TABLE Author (
            id                            SERIAL PRIMARY KEY,
            countryId                     INTEGER,
            firstname                     VARCHAR(255),
            lastname                      VARCHAR(255),
            birthdate                     DATE,
            CONSTRAINT fk_country
                FOREIGN KEY(countryId) 
                REFERENCES Country(id)
            );'''
        createTableGenreQuery = '''CREATE TABLE Genre (
            id                            SERIAL PRIMARY KEY,
            name                          VARCHAR(255)
            );'''
        createTableBookQuery = '''CREATE TABLE Book (
            id                            SERIAL PRIMARY KEY,
            genreId                     INTEGER,
            authorId                      INTEGER,
            title                         VARCHAR(255),
            description                   VARCHAR(255),
            firstReleased                 DATE,
            CONSTRAINT fk_genre
                FOREIGN KEY(genreId) 
                REFERENCES Genre(id),
            CONSTRAINT fk_author
                FOREIGN KEY(authorId) 
                REFERENCES Author(id)
            );'''
        createTableUserQuery = '''CREATE TABLE Enduser (
            id                            SERIAL PRIMARY KEY,
            username                      VARCHAR(255),
            password                      VARCHAR(255),
            createdAt                     DATE,
            isEmployee                    Boolean
            );'''
        createTableBorrowQuery = '''CREATE TABLE Borrow (
            id                            SERIAL PRIMARY KEY,
            userId                        INTEGER,
            bookId                        INTEGER,
            borrowedAt                    DATE,
            returnedAt                    DATE,
            CONSTRAINT fk_user
                FOREIGN KEY(userId) 
                REFERENCES Enduser(id),
            CONSTRAINT fk_book
                FOREIGN KEY(bookId) 
                REFERENCES Book(id)
            );'''
        
        createTablesArr = [createTableCountryQuery,createTableAuthorQuery,
                          createTableGenreQuery,createTableBookQuery,
                          createTableUserQuery,createTableBorrowQuery]

        for query in createTablesArr:
            self.db.query(query)

        insertCountryQuery = "INSERT INTO Country(name) VALUES (%s);"
        countries = ["Afghanistan","Albania","Algeria","Andorra","Angola","Antigua and Barbuda","Argentina","Armenia","Australia","Austria","Azerbaijan","The Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bhutan","Bolivia","Bosnia and Herzegovina","Botswana","Brazil","Brunei","Bulgaria","Burkina Faso","Burundi",
                    "Cabo Verde","Cambodia","Cameroon","Canada","Central African Republic","Chad","Chile","China","Colombia","Comoros","Democratic Republic of the Congo","Republic of the Congo","Costa Rica","Ivory Coast","Croatia","Cuba","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic",
                    "East Timor","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Eswatini","Ethiopia","Fiji","Finland","France","Gabon","The Gambia","Georgia","Germany","Ghana","Greece","Grenada","Guatemala","Guinea","Guinea-Bissau","Guyana","Haiti","Honduras","Hungary","Iceland","India","Indonesia","Iran","Iraq",
                    "Ireland","Israel","Italy","Jamaica","Japan","Jordan","Kazakhstan","Kenya","Kiribati","North Korea","South Korea","Kosovo","Kuwait","Kyrgyzstan","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Mauritania",
                    "Mauritius","Mexico","Federated States of Micronesia","Moldova","Monaco","Mongolia","Montenegro","Morocco","Mozambique","Myanmar","Namibia","Nauru","Nepal","Netherlands","New Zealand","Nicaragua","Niger","Nigeria","North Macedonia","Norway","Oman","Pakistan","Palau","Panama","Papua New Guinea","Paraguay","Peru","Philippines",
                    "Poland","Portugal","Qatar","Romania","Russia","Rwanda","Saint Kitts and Nevis","Saint Lucia","Saint Vincent and the Grenadines","Samoa","San Marino","Sao Tome and Principe","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","Spain",
                    "Sri Lanka","Sudan","Sudan, South","Suriname","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Togo","Tonga","Trinidad and Tobago","Tunisia","Turkey","Turkmenistan","Tuvalu","Uganda","Ukraine","United Arab Emirates","United Kingdom","United States","Uruguay","Uzbekistan","Vanuatu","Vatican City",
                    "Venezuela","Vietnam","Yemen","Zambia","Zimbabwe"]
        for country in countries:
            self.db.queryValues(insertCountryQuery,(country,))
        
        insertGenreQuery = "INSERT INTO Genre(name) VALUES (%s)"
        genres = ["Fantasy","Adventure","Romance","Contemporary","Dystopian","Mystery","Horror",
                           "Thriller","Paranormal","Science Fiction","Children’s","Cooking","Art","Health",
                           "History","Travel","Guide","Humor"]
        for genre in genres:
            self.db.queryValues(insertGenreQuery,(genre,))

    #checks if a username with the given username and password exists and in case of success it will
    #return his userId and IsEmployee status, otherwise it will return nothing
    def checkForUser(self,username,password):
        sqlStatement = "SELECT id, isEmployee FROM Enduser WHERE username=%s AND password=%s;"

        resultArray = self.db.returnQueryValues(sqlStatement,(username,password))
        if(len(resultArray) == 0):
            return None
        return resultArray[0]
    
    #checks if there is already a username with the giveb username in the database, it will return a boolean
    def usernameExists(self,username):
        sqlStatement = "SELECT id FROM Enduser WHERE username=%s;"

        resultArray = self.db.returnQueryValues(sqlStatement,(username,))
        if(len(resultArray) == 0):
            return False
        return True

    #this function creates a new user entry for the given username and password, and fills in some more information automatically
    def createUser(self,username,password):
        sqlStatement = "INSERT INTO Enduser(username,password,createdAt,isEmployee) VALUES (%s,%s,%s,%s)"

        self.db.queryValues(sqlStatement,(username,password,date.today(),False))
        return self.checkForUser(username,password)

    #Will return all countries in the database
    def getAllCountries(self):
        sqlStatement = '''SELECT id, name FROM Country ORDER BY name'''

        return self.db.returnQuery(sqlStatement)

    #function for returning all genres in the database
    def getAllGenres(self):
        sqlStatement = '''SELECT id, name FROM Genre ORDER BY name'''

        return self.db.returnQuery(sqlStatement)

    #function to return all authors in the databes with information about his country of origin
    def getAllAuthors(self):
        sqlStatement = '''SELECT Author.id, Author.firstname, Author.lastname, Country.name, Author.birthdate FROM Author
                          LEFT OUTER JOIN Country ON Author.CountryId = Country.id
                          ORDER BY Author.firstname'''

        return self.db.returnQuery(sqlStatement)

    #this function will delete the author with the specified id
    def deleteAuthor(self,authorId):
        removeFromBook = "UPDATE Book SET authorId = NULL WHERE authorId=%s;"
        deleteFromAuthor = "DELETE FROM Author WHERE id=%s;"
        self.db.queryValues(removeFromBook,(authorId,))
        self.db.queryValues(deleteFromAuthor,(authorId,))

    #creates a new author in the database with the given values
    def addAuthor(self,firstname,lastname,countryId,birthdate):
        sqlStatement = "INSERT INTO Author(firstname,lastname,countryId,birthdate) VALUES (%s,%s,%s,%s)"

        self.db.queryValues(sqlStatement,(firstname,lastname,countryId,birthdate))

    #get all book in the database with additional information about its author and genre
    def getAllBooks(self):
        sqlStatement = '''SELECT Book.id, Book.title, Book.description, Author.firstname, Author.lastname, Genre.name, Book.firstReleased
                            FROM  Book LEFT OUTER JOIN Author ON Book.authorId = Author.id
                            INNER JOIN Genre ON Book.genreID = Genre.id
                            ORDER BY Book.title'''

        return self.db.returnQuery(sqlStatement)       

    #this function will delete the book with the specified id
    def deleteBook(self,bookId):
        deleteFromBorrow = "DELETE FROM Borrow WHERE bookId=%s;"
        deleteFromBook = "DELETE FROM Book WHERE id=%s;"
        self.db.queryValues(deleteFromBorrow,(bookId,))
        self.db.queryValues(deleteFromBook,(bookId,))

    #creates a new book in the database with the given values
    def addBook(self,title,description,firstReleased,authorId,genreId):
        sqlStatement = "INSERT INTO Book(title,description,firstReleased,authorId,genreId) VALUES (%s,%s,%s,%s,%s)"

        self.db.queryValues(sqlStatement,(title,description,firstReleased,authorId,genreId))    

    #will get some information about the borrowing status of a specified user
    def getInfosAbout(self,userId):
        borrowed = self.getBorrowedBooks(userId)
        overdue = []

        for t in borrowed:
            d = t[5]
            if(d + timedelta(days=14)<date.today()):
                overdue.append(d)
        return (len(borrowed),len(overdue))

    #will return all books that a specific user is borrowing at the moment
    def getBorrowedBooks(self,userId):
        sqlStatement = '''SELECT Borrow.id, Book.title, Book.description, Author.firstname, Author.lastname, Borrow.borrowedAt
                            FROM Borrow INNER JOIN Book ON Borrow.bookId = Book.id
                            LEFT OUTER JOIN Author ON Book.authorId = Author.id
                            WHERE Borrow.userId=%s  AND Borrow.returnedAt IS NULL
                            ORDER BY Borrow.borrowedAt;'''

        borrowed = self.db.returnQueryValues(sqlStatement,(userId,))
        return borrowed

    #this will add a timestamp to a borrow 'transaction' and with it states that the book was returned
    def returnBook(self,borrowId):
        sqlStatement = "UPDATE Borrow SET returnedAt=NOW() WHERE id=%s"

        self.db.queryValues(sqlStatement,(borrowId,))

    #this function will return all books that are not currently borrowed by anyone
    def getAvailableBooks(self):
        sqlStatement = '''SELECT Book.id, Book.title, Book.description, Author.firstname, Author.lastname, Genre.name, Book.firstReleased
                            FROM  Book LEFT OUTER JOIN Author ON Book.authorId = Author.id
                            INNER JOIN Genre ON Book.genreId = Genre.id
                            WHERE Book.id NOT IN (
                                SELECT BookId FROM Borrow WHERE returnedAt IS NULL
                            )
                            ORDER BY Book.title;'''

        return self.db.returnQuery(sqlStatement)

    #returns all books that are not currently borrowed by anyone and belong to the specified genreid
    def getAvailableBooksByGenre(self,genreId):
        sqlStatement = '''SELECT Book.id, Book.title, Book.description, Author.firstname, Author.lastname, Genre.name, Book.firstReleased
                        FROM  Book LEFT OUTER JOIN Author ON Book.authorId = Author.id
                        INNER JOIN Genre ON Book.genreId = Genre.id
                        WHERE Genre.id = %s AND Book.id NOT IN (
                            SELECT BookId FROM Borrow WHERE returnedAt IS NULL
                        )
                        ORDER BY Book.title;'''

        return self.db.returnQueryValues(sqlStatement,(genreId,))

    #this will add a new entry to borrow, to signify that the specified user id borrowing the specified book
    def borrowBook(self,bookId,userId):
        sqlStatement = "INSERT INTO Borrow(UserId,BookId,BorrowedAt) VALUES (%s,%s,NOW())"

        self.db.queryValues(sqlStatement,(userId,bookId))

    #this function will change the password with the value provided of a specified user
    def changePassword(self,userId,password):
        sqlStatement = "UPDATE Enduser SET password=%s WHERE id=%s;"

        self.db.queryValues(sqlStatement,(password,userId))
