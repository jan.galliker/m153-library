import psycopg2
from config import dbConfig

class Db:
    connection = None

    #at initialization of the database it will setup the database connection
    def __init__(self):
        self.getConnection()

    #function to return the databaseconnection, if not there, it will create the connection
    def getConnection(self):
        if self.connection == None:
            self.connect()
        return self.connection

    #this function will setup the databaseconnection according to the configfile
    def connect(self):
        self.connection = None
        try:

            params = dbConfig()

            print('Connecting to the PostgreSQL database...')
            self.connection = psycopg2.connect(**params)

            cur = self.connection.cursor()
            print('PostgreSQL database version:')
            cur.execute('SELECT version()')
            dbVersion = cur.fetchone()
            print(dbVersion)

        except (Exception,psycopg2.DatabaseError) as error :
            print(error)

        finally:
            cur.close

    #this function will execute a querystring with no additional values and no return result
    def query(self,string):
        self.getConnection()
        try:
            cur = self.connection.cursor()
            cur.execute(string)
            self.connection.commit()
        except (Exception,psycopg2.DatabaseError) as error :
            print(error)
        finally:
            cur.close()

    #this function will execute a querystring with additional values and no return result
    def queryValues(self,string,values):
        self.getConnection()

        try:
            cur = self.connection.cursor()
            cur.execute(string,values)
            self.connection.commit()
        except (Exception,psycopg2.DatabaseError) as error :
            print(error)
        finally:
            cur.close()

    #this function will execute a querystring with no additional values and a return result
    def returnQuery(self,string):
        self.getConnection()

        try:
            cur = self.connection.cursor()
            cur.execute(string)
            res = cur.fetchall()
            self.connection.commit()
            return res
        except (Exception,psycopg2.DatabaseError) as error :
            print(error)
        finally:
            cur.close()

    #this function will execute a querystring with additional values and a return result
    def returnQueryValues(self,string,values):
        self.getConnection()

        try:
            cur = self.connection.cursor()
            cur.execute(string,values)
            res = cur.fetchall()
            self.connection.commit()
            return res
        except (Exception,psycopg2.DatabaseError) as error :
            print(error)
        finally:
            cur.close()

    def closeConnection(self):
        self.connection.close()
        self.connection = None


if __name__ == '__main__':
    Db.connect()