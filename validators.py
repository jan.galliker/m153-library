import re
from datetime import datetime
from prompt_toolkit.validation import Validator, ValidationError

#this is a validator for the password
class PasswordValidator(Validator):
    def validate(self, document):
        text = document.text
        if len(text)<8:
            raise ValidationError(message='Password must contain at least 8 characters',cursor_position=len(text))
        elif re.search("[a-zA-Z]",text) is None:
            raise ValidationError(message='Password must contain at least one letter',cursor_position=len(text))
        elif re.search("[0-9]",text) is None:
            raise ValidationError(message='Password must contain at least one number',cursor_position=len(text))
        elif re.search("[^a-zA-Z0-9]",text) is None:
            raise ValidationError(message='Password must contain at least one special character',cursor_position=len(text))

#this validator checks that an input has at least a certain length
class MinLengthValidator(Validator):
    def __init__(self,minlength):
        super().__init__()
        self.minlength = minlength

    def validate(self, document):
        text = document.text
        if len(text)<self.minlength:
            raise ValidationError(message='Input must contain at least %s characters' % self.minlength ,cursor_position=len(text))

#this validator checks that the input is a valid date
class DateValidator(Validator):
    def validate(self, document):
        text = document.text
        try:
            datetime.strptime(text, '%d.%m.%Y').date()
        except:
            raise ValidationError(message='Invalid format or invalid Date',cursor_position=len(text))
